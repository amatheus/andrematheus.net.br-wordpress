<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package andrematheus.net.br
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"
            integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header id="main_header">
    <h1 id="hero" class="site-title"><?php bloginfo( 'name' ); ?></h1>
	<?php
	$menuParameters = array(
		'container'  => false,
		'items_wrap' => '%3$s',
		'depth'      => 0,
	);
	?>
    <nav id="menu">
		<?php
		wp_nav_menu( $menuParameters );
		?>
    </nav>
    <nav id="popup">
        <div id="mask"></div>
        <div id="contents">
			<?php
			wp_nav_menu( $menuParameters );
			?>
        </div>
    </nav>
    <a id="popup" href="javascript:showNavigation();">Navegação</a>
</header>