function showNavigation() {
    $("nav#popup").show();
}

function hideNavigation() {
    $("nav#popup").hide();
}

$(document).ready(function () {
    var fixedElements = {};

    $('.fixed-element').each(function (idx) {
        console.log('Got ');
        console.log($(this).position());
        if ($(this).attr("id")) {
            fixedElements[$(this).attr("id")] = $(this).position()['top'];
        }
    });

    $(window).on('scroll', function () {
        $.each(fixedElements, function (idx, ob) {
            console.log("setting...");
            $("#" + idx).position()['top'] = ob;
        });
    });

    $("nav#popup > div#mask").click(hideNavigation);
});