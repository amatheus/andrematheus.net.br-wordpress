<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package andrematheus.net.br
 */

?>

<footer id="colophon" class="site-footer">
    © 2016 André Roque Matheus • <a href="/sobre/">Sobre esse site</a>
</footer>

<?php wp_footer(); ?>

</body>
</html>
